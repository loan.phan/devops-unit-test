#!bin/bash

git init
npm init -y
npm install express --save
npm install --save-dev mocha chai

mkdir src
touch src/main.js
touch src/mylib.js

mkdir test
touch test/mylib.test.js

echo "node_modules">.gitignore
echo ".DS_Store">>.gitignore
