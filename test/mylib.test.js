const expect = require('chai').expect;
const mylib = require('../src/mylib.js');

describe('Unit testing mylib.js', () => {
    before(() => {
        console.log('Testing starts.')
    });
    // Testing function add
    it('Add function can add 1 and 1 together', () => {
        const result = mylib.add(1, 1);
        expect(result).to.equal(2);
    });
    // Testing function subtract
    it('Subtract function can subtract 1 from 2', () => {
        const result = mylib.subtract(2,1);
        expect(result).to.equal(1);
    } );
    it('Multiple function can multiple 1 and 2', () => {
        const result = mylib.multiply(1,2);
        expect(result).to.equal(2);
    });
    it('Divide function can devide 1 by 1', () => {
        const result = mylib.divide(1,1);
        expect(result).to.equal(1);
    });
    it('Divide function can throw an ZeroDivision error when dividing by 0',() => {
        const result = () => mylib.divide(10, 0);
        expect(result).to.throw('Division by zero is not allowed.');
    });
    after(() => {
        console.log('Testing ends.');
    })

});