// Function to add two numbers
function add(a, b) {
  return a + b;
}

// Function to subtract a number from another
function subtract(a, b) {
  return a - b;
}

// Function to multiply two numbers
function multiply(a, b) {
  return a * b;
}

// Function to divide a number by another
function divide(a, b) {
  if (b === 0) {
    throw new Error("Division by zero is not allowed.");
  }
  return a / b;
}

// Export the functions
module.exports = {
  add,
  subtract,
  multiply,
  divide,
};
